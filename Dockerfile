# base image
ARG ARCH=amd64
FROM $ARCH/alpine:3

# args
ARG VCS_REF
ARG BUILD_DATE

# labels
LABEL maintainer="Michael Balser <michael@balser.cc>" \
  org.label-schema.schema-version="1.0" \
  org.label-schema.name="michaelbalser/avahi" \
  org.label-schema.description="Simple Avahi reflector docker image" \
  org.label-schema.version="0.1" \
  org.label-schema.url="https://hub.docker.com/repository/docker/michaelbalser/avahi" \
  org.label-schema.vcs-url="https://gitlab.com/mbalser1/avahi" \
  org.label-schema.vcs-ref=$VCS_REF \
  org.label-schema.build-date=$BUILD_DATE

# install packages
RUN apk --no-cache --no-progress add avahi avahi-tools tini

# remove default services
RUN rm /etc/avahi/services/*

# disable d-bus
RUN sed -i 's/.*enable-dbus=.*/enable-dbus=no/' /etc/avahi/avahi-daemon.conf \
  && sed -i 's/.*enable-reflector=.*/enable-reflector=yes/' /etc/avahi/avahi-daemon.conf

COPY files /
RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/sbin/tini", "--", "/docker-entrypoint.sh"]

# default command
CMD ["avahi-daemon"]

# volumes
VOLUME ["/etc/avahi/services"]
